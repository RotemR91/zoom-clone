
import Router, {useRouter} from 'next/router';
import { useEffect, useRef, useState } from 'react';
import supabase from '../supabase';
import { Button, IconVideo, IconVideoOff, IconMic, IconMicOff, IconMessageSquare, IconMonitor, IconLogOut } from "@supabase/ui";
import { createSocketConnectionInstance } from '../utils/socketConnection'

export default function Room () {
  const router = useRouter()
  const roomId = router.asPath.split('roomId=')[1]

  let socketInstance = useRef(null);
  const [micStatus, setMicStatus] = useState(true);
  const [camStatus, setCamStatus] = useState(true);
  const [streaming, setStreaming] = useState(false);
  const [chatToggle, setChatToggle] = useState(false);
  const [displayStream, setDisplayStream] = useState(false);
  const [front, setFront] = useState(false);
  const [messages, setMessages] = useState([]);
  const [once, setOnce] = useState(false);

  useEffect(() => {
    return () => {
      if (socketInstance?.current) {
        socketInstance.current.destoryConnection();
      }
    }
  }, []);

  useEffect(() => {
    if (!supabase.auth.user()) {
      Router.push({ pathname: '/login' })
      return
    }
    socketInstance.current = createSocketConnectionInstance({
      updateInstance: updateFromInstance,
      params: { roomId, id: supabase.auth.user().id }
    });
  }, [roomId]);

  const updateFromInstance = (key, value) => {
    if (key === 'streaming') setStreaming(value);
    if (key === 'message') setMessages([...value]);
    if (key === 'displayStream') setDisplayStream(value);
  }

  const handleDisconnect = () => {
    socketInstance.current.destoryConnection();
    Router.push('/');
  }

  const handleMyMic = () => {
    const { getMyVideo, reInitializeStream } = socketInstance.current;
    const myVideo = getMyVideo();
    if (myVideo) myVideo.srcObject.getAudioTracks().forEach((track) => {
        if (track.kind === 'audio')
            // track.enabled = !micStatus;
            micStatus ? track.stop() : reInitializeStream(camStatus, !micStatus);
    });
    setMicStatus(!micStatus);
  }

  const handleMyCam = () => {
    if (!displayStream) {
      const { toggleVideoTrack } = socketInstance.current;
      toggleVideoTrack({ video: !camStatus, audio: micStatus });
      setCamStatus(!camStatus);
    }
  }

  const toggleScreenShare = () => {
    const { reInitializeStream, toggleVideoTrack } = socketInstance.current;
    displayStream && toggleVideoTrack({video: false, audio: true});
    reInitializeStream(false, true, !displayStream ? 'displayMedia' : 'userMedia').then(() => {
      setDisplayStream(!displayStream);
      setCamStatus(false);
    });
  }

  return (
    <div className='container'>
      <div id="room-container" className='columns-2' />
      <div className="flex space-x-1">
        <Button type="outline" icon={!camStatus ? <IconVideoOff /> : <IconVideo />} onClick={() => handleMyCam()}>Video</Button>
        <Button type="outline" icon={!micStatus ? <IconMicOff /> : <IconMic />} onClick={() => handleMyMic()}>Mic</Button>
        <Button type="outline" icon={<IconMessageSquare />} onClick={() => setChatToggle(!chatToggle)}>Chat</Button>
        <Button type="outline" icon={<IconMonitor />} onClick={() => toggleScreenShare()}>Share</Button>
        <Button type="outline" icon={<IconLogOut />} onClick={() => handleDisconnect()}>Disconnect</Button>
      </div>
    </div>
  )
}