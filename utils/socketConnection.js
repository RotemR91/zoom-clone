import io from 'socket.io-client';
import Peer from 'peerjs';

let socketInstance = null;
let peers = {};

class SocketConnection {
	videoContainer = {};
	message = [];
	settings;
	streaming = false;
	myPeer;
	socket;
	isSocketConnected = false;
	isPeersConnected = false;
	myID = '';
	
	constructor(settings) {
		this.settings = settings;
		this.myPeer = initializePeerConnection(settings.params.id);
		this.socket = initializeSocketConnection();
		this.socket.auth = {userID: settings.params.id}
		this.socket.connect()
		this.myID = settings.params.id
		if (this.socket) this.isSocketConnected = true;
		if (this.myPeer) this.isPeersConnected = true;
		this.initializePeersEvents();
		this.initializeSocketEvents();
	}
	
	initializeSocketEvents = () => {
		this.socket.on('connect', (userID) => {
				console.log('user connected ', userID);
		});
		this.socket.on('user-disconnected', (userID) => {
				console.log('user disconnected-- closing peers', userID);
				peers[userID] && peers[userID].close();
				this.removeVideo(userID);
		});
		this.socket.on('disconnect', (reason) => {
				console.log('socket disconnected --', reason);
		});
		this.socket.on('error', (err) => {
				console.log('socket error --', err);
		});
		this.socket.on('new-broadcast-messsage', (data) => {
				this.message.push(data);
				this.settings.updateInstance('message', this.message);
		});
		this.socket.on('display-media', (data) => {
				if (data.value) checkAndAddClass(this.getMyVideo(data.userID), 'displayMedia');
				else checkAndAddClass(this.getMyVideo(data.userID), 'userMedia');
		});
		this.socket.on('user-video-off', (data) => {
		    changeMediaView(data.id, data.status);
		});
	}
	
	initializePeersEvents = () => {
		const that = this
		this.myPeer.on('open', async () => {
			const { roomId, id } = that.settings.params;
			const userData = {
				userID: id,
				roomID: roomId
			}
			console.log('peers established and joined room', userData);
			that.socket.emit('join-room', userData);
			await that.setNavigatorToStream();
		});
		this.myPeer.on('error', (err) => {
			console.log('peer connection error', err);
			that.myPeer.disconnect();
			that.myPeer.reconnect();
		})
	}
	
	setNavigatorToStream = async () => {
		const stream = await this.getVideoAudioStream()
		console.log('navigation to streanm', stream)
		if (stream) {
			this.streaming = true;
			this.settings.updateInstance('streaming', true);
			this.createVideo({ id: this.myID, stream });
			this.setPeersListeners(stream);
			this.newUserConnection(stream);
		}
	}
	
	getVideoAudioStream = (video=true, audio=true) => {
			const front = this.settings.params.front
			if (navigator.mediaDevices === undefined) {
				navigator.mediaDevices = {};
			}
			if (navigator.mediaDevices.getUserMedia === undefined) {
				navigator.mediaDevices.getUserMedia = function(constraints) {
					// First get ahold of the legacy getUserMedia, if present
					var getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
			
					// Some browsers just don't implement it - return a rejected promise with an error
					// to keep a consistent interface
					if (!getUserMedia) {
						return Promise.reject(new Error('getUserMedia is not implemented in this browser'));
					}
			
					// Otherwise, wrap the call to the old navigator.getUserMedia with a Promise
					return new Promise(function(resolve, reject) {
						getUserMedia.call(navigator, constraints, resolve, reject);
					});
				}
			}
			
			return navigator.mediaDevices.getUserMedia({
				video: video ? {
					frameRate: {ideal: 30, min: 12},
					facingMode: front ? "user" : "environment",
					noiseSuppression: true,
					width: {min: 640, ideal: 1280, max: 1920},
					height: {min: 480, ideal: 720, max: 1080}
				} : false,
				audio: audio,
			});
	}
	
	setPeersListeners = (stream) => {
		this.myPeer.on('call', (call) => {
			call.answer(stream);
			call.on('stream', (userVideoStream) => {
				this.createVideo({ id: call.metadata.id, stream: userVideoStream });
			});
			call.on('close', () => {
				console.log('closing peers listeners', call.metadata.id);
				this.removeVideo(call.metadata.id);
			});
			call.on('error', () => {
				console.log('peer error ------');
				this.removeVideo(call.metadata.id);
			});
			peers[call.metadata.id] = call;
		});
	}
	
	newUserConnection = (stream) => {
		this.socket.on('new-user-connect', (userData) => {
			console.log('New User Connected', userData);
			this.connectToNewUser(userData, stream);
		});
	}
	
	connectToNewUser(userData, stream) {
		const { userID } = userData;
		const call = this.myPeer.call(userID, stream, { metadata: { id: this.myID } });
		call.on('stream', (userVideoStream) => {
			this.createVideo({ id: userID, stream: userVideoStream });
		});
		call.on('close', () => {
			console.log('closing new user', userID);
			this.removeVideo(userID);
		});
		call.on('error', () => {
			console.log('peer error ------')
			this.removeVideo(userID);
		})
		peers[userID] = call;
	}
	
	boradcastMessage = (message) => {
		this.message.push(message);
		this.settings.updateInstance('message', this.message);
		this.socket.emit('broadcast-message', message);
	}
	
	createVideo = (createObj) => {
		console.log('creating video', createObj.id)
		if (!this.videoContainer[createObj.id]) {
			this.videoContainer[createObj.id] = {
					...createObj,
			};
			const roomContainer = document.getElementById('room-container');
			const videoContainer = document.createElement('div');
			const video = document.createElement('video');
			video.srcObject = this.videoContainer[createObj.id].stream;
			video.id = createObj.id;
			video.autoplay = true;
			if (this.myID === createObj.id) video.muted = true;
			videoContainer.appendChild(video)
			roomContainer.append(videoContainer);
		} else {
			console.log('exists', createObj.id)
			document.getElementById(createObj.id).srcObject = createObj.stream;
		}
	}
	
	reInitializeStream = (video, audio, type='userMedia') => {
		const media = type === 'userMedia' ? this.getVideoAudioStream(video, audio) : navigator.mediaDevices.getDisplayMedia();
		return new Promise((resolve) => {
			media.then((stream) => {
				const myVideo = this.getMyVideo();
				if (type === 'displayMedia') {
					this.toggleVideoTrack({audio, video});
					this.listenToEndStream(stream, {video, audio});
					this.socket.emit('display-media', true);
				}
				checkAndAddClass(myVideo, type);
				this.createVideo({ id: this.myID, stream });
				replaceStream(stream);
				resolve(true);
			});
		});
	}
	
	removeVideo = (id) => {
		delete this.videoContainer[id];
		const video = document.getElementById(id);
		if (video) video.remove();
	}
	
	destoryConnection = () => {
		if (this.videoContainer[this.myID]) {
			const myMediaTracks = this.videoContainer[this.myID].stream.getTracks();
			myMediaTracks.forEach((track) => {
					track.stop();
			})
			socketInstance.socket.disconnect();
			this.myPeer.destroy();
		}
	}
	
	getMyVideo = (id=this.myID) => {
		return document.getElementById(id);
	}
	
	listenToEndStream = (stream, status) => {
		const videoTrack = stream.getVideoTracks();
		if (videoTrack[0]) {
			videoTrack[0].onended = () => {
				this.socket.emit('display-media', false);
				this.reInitializeStream(status.video, status.audio, 'userMedia');
				this.settings.updateInstance('displayStream', false);
				this.toggleVideoTrack(status);
			}
		}
	};
	
	toggleVideoTrack = (status) => {
		const myVideo = this.getMyVideo();
		if (myVideo && !status.video) myVideo.srcObject.getVideoTracks().forEach((track) => {
			if (track.kind === 'video') {
				track.enabled = status.video;
				this.socket.emit('user-video-off', {id: this.myID, status: true});
				changeMediaView(this.myID, true);
				!status.video && track.stop();
			}
		});
		else if (myVideo) {
			this.socket.emit('user-video-off', {id: this.myID, status: false});
			changeMediaView(this.myID, false);
			this.reInitializeStream(status.video, status.audio);
		}
	}
	
	toggleAudioTrack = (status) => {
		const myVideo = this.getMyVideo();
		if (myVideo) myVideo.srcObject.getAudioTracks().forEach((track) => {
			if (track.kind === 'audio')
				track.enabled = status.audio;
			status.audio ? this.reInitializeStream(status.video, status.audio) : track.stop();
		});
	}	
}

const initializePeerConnection = (id) => {
	return new Peer(id, {
		host: 'zoom-peer.rotemrevivo.com',
		path: '/',
		secure: true,
	});
}

const initializeSocketConnection = () => {
	return io("https://zoom-io.rotemrevivo.com", {
		reconnection: false,
		rejectUnauthorized: false
	});
}

const replaceStream = (mediaStream) => {
	Object.values(peers).map((peer) => {
		peer.peerConnection.getSenders().map((sender) => {
			if(sender.track.kind == "audio") {
				if(mediaStream.getAudioTracks().length > 0){
					sender.replaceTrack(mediaStream.getAudioTracks()[0]);
				}
			}
			if(sender.track.kind == "video") {
				if(mediaStream.getVideoTracks().length > 0){
					sender.replaceTrack(mediaStream.getVideoTracks()[0]);
				}
			}
		});
	})
}

const checkAndAddClass = (video, type='userMedia') => {
	if (video.classList.length === 0 && type === 'displayMedia')
		video.classList.add('display-media');
	else
		video.classList.remove('display-media');
}

const changeMediaView = (userID, status) => {
	const userVideoDOM = document.getElementById(userID);
	if (status) {
		const clientPosition = userVideoDOM.getBoundingClientRect();
		const createdCanvas = document.createElement("SPAN");
		createdCanvas.className = userID;
		createdCanvas.style.position = 'absolute';
		createdCanvas.style.left = `${clientPosition.left}px`;
		createdCanvas.style.top = `${clientPosition.top}px`;
		// createdCanvas.style.width = `${userVideoDOM.videoWidth}px`;
		// createdCanvas.style.height = `${clientPosition.height}px`;
		createdCanvas.style.width = '100%';
		createdCanvas.style.height = '100%';
		createdCanvas.style.backgroundColor = 'green';
		userVideoDOM.parentElement.appendChild(createdCanvas);
	} else {
		const canvasElement = document.getElementsByClassName(userID);
		if (canvasElement[0]) canvasElement[0].remove();
	}
}

export function createSocketConnectionInstance(settings={}) {
  return socketInstance = new SocketConnection(settings);
}