export default function Footer () {
  return (
    <footer className="h-1/5 border-t-2 sticky">
      <span className="flex justify-center mx-auto">
        Powered by Rotem Revivo 
      </span>
    </footer>
  )
}