import { Auth, Typography, Button } from '@supabase/ui'

import supabase from "../supabase"

const Container = (props) => {
  const { user } = Auth.useUser()
  if (user)
    return (
      <>
        <Typography.Text>Welcome, {user.email}</Typography.Text>
        <Button type="primary" className="mt-2" block onClick={() => props.supabaseClient.auth.signOut()}>
          Sign out
        </Button>
      </>
    )
  return props.children
}

export default function AuthBasic() {
  return (
    <>
      <div className="center-flex">
        <h1 className="text-5xl font-medium leading-tight mt-0 mb-2 text-black">{!supabase.auth.user() ? 'Login' : 'Logged in' }</h1>
        <Auth.UserContextProvider supabaseClient={supabase}>
          <Container supabaseClient={supabase}>
            <Auth supabaseClient={supabase} />
          </Container>
        </Auth.UserContextProvider>
      </div>
    </>
  )
}