module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        "supabase-green": "24b47e"
      }
    },
  },
  plugins: [
    require('@tailwindcss/typography')
  ]
}
