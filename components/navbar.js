
import { Button, IconUser, IconHelpCircle } from '@supabase/ui'

import Link from 'next/link'
import supabase from '../supabase'

export default function NavBar () {
  const user = supabase.auth.user()
  const links = [
    {
      to: '/about',
      text: 'About',
      _class: "text-red",
      icon: <IconHelpCircle size="small" />
    },
    {
      to: '/login',
      text: 'Login',
      _class: '',
      icon: <IconUser size="small" />
    }
  ]

  return (
    <nav className="px-2 sm:px-4 py-2.5 dark:bg-zinc-900">
      <div className="container flex flex-wrap justify-between items-center mx-auto">
        <Link href="/">
          <a>
            <span className="self-center text-lg font-semibold dark:text-white">Next Supabase Zoom</span>
          </a>
        </Link>
        <div className="md:block md:w-auto space-x-1">
          {links.map(link => {
            return (
              <Link href={link.to} key={link.text} passHref>
                <Button type="primary" iconRight={link.icon}>{link.text}</Button>
              </Link>
            )
          })}
        </div>
      </div>
    </nav>
  )
}

